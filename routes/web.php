<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\EditItemController;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


//HOME
Route::GET('/home', 'HomeController@index')->name('home');
Route::POST('/home','HomeController@store')->name('home.store');

Route::GET('/home/edit/{id}', 'HomeController@edit')->name('home.edit');
Route::PUT('/home/{id}', 'HomeController@update')->name('home.update');

Route::DELETE('/home/{id}', 'HomeController@destroy')->name('home.destroy');

Route::POST('/home/filter', 'HomeController@filter')->name('home.filter');



// SETTINGS
Route::GET('/settings', 'SettingsController@index')->name('settings');
Route::GET('/settings/account', 'SettingsController@account')->name('account');
Route::PUT('/settings/{id}', 'SettingsController@update')->name('settings.update');
Route::GET('/settings/test', 'SettingsController@test')->name('test');


Route::GET('home/{lang}', function ($locale) {
    if (! in_array($locale, ['en', 'nl', 'fr'])) {
        abort(400);
    }

    App::setLocale($locale);
    session()->put('locale', $locale);
    $items = DB::table("fridge")->where("user_id",Auth::id())->get();
    foreach ($items as $item){
        $item->entry_date =  date('d-m-Y', $item->entry_date);
        $item->ex_date =  date('d-m-Y', $item->ex_date);
    }

    if(Auth::check()){
        return view('home',['items' => $items]);
    }else{
        return view('welcome');
    }

});



