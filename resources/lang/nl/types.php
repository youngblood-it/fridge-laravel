<?php
/**
 * Created by PhpStorm.
 * User: dennis-pc
 * Date: 2-11-2020
 * Time: 16:05
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'Show all' => 'Laat alles zien',
    'Bread' => 'Brood',
    'Dough' => 'Deeg',
    'Fruit' => 'Fruit',
    'Vegetable' => 'Groente',
    'Chicken' => 'Kip',
    'Herbs' => 'Kruide',
    'Pasta' => 'Pasta',
    'Rice' => 'Rijst',
    'Saus' => 'Saus',
    'Snacks' => 'Snacks',
    'Soup' => 'Soep',
    'Meat' => 'Vlees',
    'Ice' => 'Ijs',


];
