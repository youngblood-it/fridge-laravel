<?php
/**
 * Created by PhpStorm.
 * User: dennis-pc
 * Date: 2-11-2020
 * Time: 12:59
 */


return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'name' => 'Name',
    'type' => 'Type',
    'entry date' => 'Entry Date',
    'execution date' => 'Execution Date',
    'close' => 'Close',
    'submit' => 'Submit',


];
