<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @stack('head')
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>
    <div id="app">

        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container-fluid">
                @guest
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Fridge') }}
                    </a>
                @else
                    <a class="navbar-brand" href="{{ route('home')}}">
                        {{ config('app.name', 'Fridge') }}
                    </a>
                @endguest
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            @php $locale = session()->get('locale'); @endphp
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    @switch($locale)
                                        @case('en')
                                        <img class="lang_flag" src="{{asset('assets/flags/uk.png')}}"> English
                                        @break
                                        @case('nl')
                                        <img class="lang_flag" src="{{asset('assets/flags/nl.png')}}"> Dutch
                                        @break
                                        @case('de')
                                        <img class="lang_flag" src="{{asset('assets/flags/de.png')}}"> German
                                        @break
                                        @case('in')
                                        <img class="lang_flag" src="{{asset('assets/flags/in.png')}}"> Hindi
                                        @break
                                        @case('fr')
                                        <img class="lang_flag" src="{{asset('assets/flags/fr.png')}}"> French
                                        @break
                                        @case('es')
                                        <img class="lang_flag" src="{{asset('assets/flags/es.png')}}"> Spanish
                                        @break
                                        @case('ch')
                                        <img class="lang_flag" src="{{asset('assets/flags/ch.png')}}"> Chinese
                                        @break
                                        @default
                                        <img class="lang_flag" src="{{asset('assets/flags/uk.png')}}"> English
                                    @endswitch
                                    <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="/home/en"><img class="lang_flag" src="{{asset('assets/flags/uk.png')}}"> English</a>
                                    <a class="dropdown-item" href="/home/nl"><img class="lang_flag" src="{{asset('assets/flags/nl.png')}}"> Dutch</a>
                                    {{--<a class="dropdown-item" href="home/de"><img src="{{asset('img/de.png')}}"> German</a>--}}
                                    {{--<a class="dropdown-item" href="home/in"><img src="{{asset('img/in.png')}}"> Hindi</a>--}}
                                    {{--<a class="dropdown-item" href="home/fr"><img src="{{asset('img/fr.png')}}"> French</a>--}}
                                    {{--<a class="dropdown-item" href="home/es"><img src="{{asset('img/es.png')}}"> Spanish</a>--}}
                                    {{--<a class="dropdown-item" href="home/ch"><img src="{{asset('img/ch.png')}}"> Chinese</a>--}}
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                    <a class="dropdown-item" href="{{ route('home') }}">
                                        {{ __('home') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('settings') }}">
                                        {{ __('settings') }}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main>
            @yield('content')
        </main>
    </div>
</body>
</html>



