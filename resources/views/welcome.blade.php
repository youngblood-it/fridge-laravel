<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>


        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <!-- Latest compiled JavaScript -->

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <nav class="nav_welcome">
        @if (Route::has('login'))
            <div class="top-right links">
                @auth
                    <a href="{{ url('/home') }}">Home</a>
                @else
                    <a href="{{ route('login') }}">Login</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}">Register</a>
                    @endif
                @endauth
            </div>
        @endif
        </nav>
        <header class="header_welcome_container container-fluid">
            <div class="row justify-content-center ">
                <h1 class="header_welcome_header_text ">Fridge App</h1>
            </div>
        </header>
        <main class="main_content_container">
            <section class="section_welcome_info_container container">
                <div class="row">
                    <div class="col-12 col-md-6 welcome_info_container">
                        <h4 class="welcome_info_header_text ">What is Fridge</h4>
                        <p class="welcome_info_text">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                            Aenean commodo ligula eget dolor. Aenean massa.
                            Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                            Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
                        </p>
                    </div>
                    <div class="col-12 col-md-6 welcome_info_container">
                        <div class="welcome_info_image_container">
                            <img class="welcome_info_image" src="{{asset('assets/dashboard.png')}}">
                        </div>
                    </div>
                </div>
            </section>
            <section class="section_poi_info_container container">
                <div class="row">
                    <div class="col-12 col-md-4 poi_info_container poi_left_container">
                        <div class="row">
                            <div class="col-12 col-md-3 poi_icon">
                                <i class="fa fa-snowflake-o" aria-hidden="true"></i>
                            </div>
                            <div class="col-12 col-md-9 poi_title">
                                <h3>Dashboard</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 poi_text">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                    Aenean commodo ligula eget dolor.
                                    Aenean massa.
                                    Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 poi_info_container poi_center_container">
                        <div class="row">
                            <div class="col-12 col-md-3 poi_icon">
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            </div>
                            <div class="col-12 col-md-9 poi_title">
                                <h3>Weekly update</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 poi_text">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                    Aenean commodo ligula eget dolor.
                                    Aenean massa.
                                    Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4  poi_info_container poi_right_container">
                        <div class="row">
                            <div class="col-12 col-md-3 poi_icon">
                                <i class="fa fa-user-o" aria-hidden="true"></i>
                            </div>
                            <div class="col-12 col-md-9 poi_title">
                                <h3>test</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 poi_text">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                    Aenean commodo ligula eget dolor.
                                    Aenean massa.
                                    Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            {{--<div class="position-ref full-height">--}}

                {{--<div class="container">--}}
                    {{--<div class="content">--}}
                        {{--<div class="title m-b-md">--}}
                            {{--Laravel--}}
                        {{--</div>--}}

                        {{--<div class="links">--}}
                            {{--<a href="https://laravel.com/docs">Docs</a>--}}
                            {{--<a href="https://laracasts.com">Laracasts</a>--}}
                            {{--<a href="https://laravel-news.com">News</a>--}}
                            {{--<a href="https://blog.laravel.com">Blog</a>--}}
                            {{--<a href="https://nova.laravel.com">Nova</a>--}}
                            {{--<a href="https://forge.laravel.com">Forge</a>--}}
                            {{--<a href="https://vapor.laravel.com">Vapor</a>--}}
                            {{--<a href="https://github.com/laravel/laravel">GitHub</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </main>
        <footer class="footer_container">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-4">
                        <div class="row">
                            <div class="col-12 footer_contact_header" >
                                <h5> Contact</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-2 footer_contact_section_header footer_mail_header" >
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            </div>
                            <div class="col-12 col-md-10 footer_contact_section_header footer_mail" >
                                <a href="mailto:info@fridge.com">info@fride.com</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">

                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="row">
                            <div class="col-12 footer_contact_header" >
                                <h5> Pages</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-12 footer_pages" >
                                <a href="/login">Login</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-md-12 footer_pages" >
                                <a href="/register">Register</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>
