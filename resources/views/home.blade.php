@extends('layouts.app')

@section('content')
<div class="container-fluid dashboard">
    <div class="row">
        <section id="dashboard_side_nav" class="col-12 col-md-3 col-xl-2 col-sm-12 dashboard_side_nav ">
            <div class="row">
                <section class="col-12 col-xl-12 dashboard_header">
                    <div class="row">
                        <div class="col-12 col-xl-12 add_item_container">
                            {{--<button class="add_item_btn" onClick={this.showNewItem}>+</button>--}}
                            @include('partials._addItem',$items)
                        </div>
                    </div>
                    <div class="row">
                        <hr>
                    </div>
                    <div class="row">
                        <p class="col-12 filter_header">Filters</p>
                    </div>
                    <div class="row">
                        <form class="col-12 col-md-12 dashboard_filter_container" method="POST" action="{{route('home.filter')}}" >
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-12 col-sm-10 col-md-12 col-xl-12 sort_filter_container">
                                    <select id="sort_filter" class="sort_filer" type="text" name="type_filter">
                                        <option class="new_item_type_option" value="All">{{ __('types.Show all')}}</option>
                                        @include("partials.filters._typeSelectOptions")
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-2 col-sm-2 col-md-3">
                                    <button type="submit" class="filter_btn  btn btn-primary">
                                        FILTER
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </section>
        <section class="col-12 col-sm-12 col-md-9  col-xl-10 dashboard_content ">
            <div class="row">
                <div class="container">


                    <div class="row">


                        <section id="item_list_container" class="col-12 col-xl-12 item_list_container">
                            @include('partials._item',$items)
                        </section>
                    </div>

                </div>
            </div>
        </section>

    </div>



</div>
@endsection
