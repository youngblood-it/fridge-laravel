
@foreach ($items as $item)

    @include('partials._editItem',["editItem"=>$item])


    <section class="section_item col-12">
        <div class="row">
            <div class="item_container col-12">
                <div class="row">
                    <div class="item_label_container col-4 col-md-3 col-xl-2">
                        <div class="row">
                            <div class="col-12">
                                <p>{{__('item.name')}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p>{{__('item.type')}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p>{{__('item.entry date')}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p>{{__('item.execution date')}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="item_content_container  offset-1 col-5 offset-md-1 col-md-6 offset-xl-1 col-xl-7">
                        <div class="row">
                            <div class="col-12">

                                <p class="item_content">{{$item->product_name}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p class="item_content">{{ __('types.'.$item->product_type)}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p class="item_content">{{$item->entry_date}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <p class="item_content">{{$item->ex_date}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="item_control_container col-2 2col-md-2">
                        <div class="row">
                            <div class="col-12  offset-md-4 col-md-4 offset-xl-4 col-xl-4">

                                    <button type="button" class="btn btn-primary edit_item_btn open_modal" data-toggle="modal" data-target="#editItemModal_{{$item->id}}" value="{{$item->id}}" data-target-id="{{$item->id}}">
                                        <svg width="20px" height="20px" viewBox="0 0 16 16"
                                             class="bi bi-pencil" fill="currentColor"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd"
                                                  d="M11.293 1.293a1 1 0 0 1 1.414 0l2 2a1 1 0 0 1 0 1.414l-9 9a1 1 0 0 1-.39.242l-3 1a1 1 0 0 1-1.266-1.265l1-3a1 1 0 0 1 .242-.391l9-9zM12 2l2 2-9 9-3 1 1-3 9-9z"/>
                                            <path fill-rule="evenodd"
                                                  d="M12.146 6.354l-2.5-2.5.708-.708 2.5 2.5-.707.708zM3 10v.5a.5.5 0 0 0 .5.5H4v.5a.5.5 0 0 0 .5.5H5v.5a.5.5 0 0 0 .5.5H6v-1.5a.5.5 0 0 0-.5-.5H5v-.5a.5.5 0 0 0-.5-.5H3z"/>
                                        </svg>
                                    </button>

                            </div>
                            <div class="col-12  offset-md-4 col-md-4 offset-xl-0 col-xl-4">
                                <form class="app-form" id="modal-form" method="POST" action="{{route('home.destroy', ['id' => $item->id])}}" >
                                    {{ csrf_field() }}
                                    @method('DELETE')
                                    <button type="submit" class=" btn btn-primary delete_item_btn" >
                                        <svg width="20px" height="20px" viewBox="0 0 16 16"
                                             class="bi bi-trash" fill="currentColor"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                    d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                            <path fill-rule="evenodd"
                                                  d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                        </svg>
                                    </button>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endforeach