
{{--TODO place in js file--}}
{{--@push('head')--}}
    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>--}}
    {{--<script>--}}
        {{--let item_id = "";--}}

        {{--$(document).on('click','.open_modal',function(){--}}
            {{--var url = "/home/edit";--}}
            {{--var tour_id= $(this).val();--}}
            {{--$.get(url + '/' + tour_id, function (data) {--}}
                {{--//success data--}}
                {{--console.log(data);--}}
                {{--$('#edit_item_name').val(data[0]['product_name']);--}}
                {{--$('#edit_item_type').val(data[0]['product_type']);--}}
                {{--$('#edit_item_entry_date').val(data[0]['entry_date']);--}}
                {{--$('#edit_item_execution_date').val(data[0]['ex_date']);--}}

                {{--item_id = data[0]['id'];--}}
                {{--//$('#myModal').modal('show');--}}
            {{--})--}}
        {{--});--}}


    {{--</script>--}}
{{--@endpush--}}



<div class="edit_item_modal modal fade" id="editItemModal_{{$editItem->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title" id="exampleModalLabel">Edit Item</h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form class="edit_item_form app-form" id="modal-form" method="POST" action="{{route('home.update', ['id' => $editItem->id])}}" >
                {{ csrf_field() }}
                @method('PUT')
                <div class="modal-body">
                    <div id="new_item_container" class="">
                        <div class="row">
                            <div class="container new_item_container">
                                <div class="row justify-content-center">
                                    <div class="col-12 col-xl-8 new_item_form">
                                        <div class="row new_item_row">
                                            <label class="new_item_title col-md-5">{{__('item.name')}}</label>
                                            <input id="edit_item_name" class="new_item_input_text col-md-7" type="text" name="name" value="{{$editItem->product_name}}"/>
                                        </div>
                                        <div class="row new_item_row">
                                            <label class="new_item_title col-md-5">{{__('item.type')}}</label>
                                            <select id="edit_item_type" class="new_item_type_select col-md-7" type="text" name="type">
                                                @include("partials.filters._typeSelectOptions")
                                            </select>
                                        </div>
                                        <div class="row new_item_row">
                                            <label class="new_item_title col-md-5">{{__('item.entry date')}}</label>
                                            <input id="edit_item_entry_date" class="new_item_input_date col-md-7" type="date" name="entryDate" value="{{$editItem->entry_date}}"/>
                                        </div>
                                        <div class="row new_item_row">
                                            <label class="new_item_title col-md-5">{{__('item.execution date')}}</label>
                                            <input id="edit_item_execution_date" class="new_item_input_date col-md-7" type="date" name="executionDate" value="{{$editItem->ex_date}}"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class=" btn btn-secondary btn_close" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary btn_save">
                </div>
            </form>
        </div>
    </div>
</div>


