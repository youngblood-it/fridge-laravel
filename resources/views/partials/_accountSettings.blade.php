@extends('layouts.app')



@section('content')
    <div class="container-fluid settings">
        <div class="row">
            <section class="col-12 col-md-2 settings_side_nav ">
                <div class="row">
                    <div class="col-12 col-md-12 navbar_settings" >
                        <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('account') }}">account</a>
                            </li>
                            {{--<li class="nav-item">--}}
                                {{--<a class="nav-link" href="{{ route('test') }}">test</a>--}}
                            {{--</li>--}}
                        </ul>
                    </div>
                </div>
            </section>
            <section class="col-12 col-md-10 settings_content ">
                <div class="row">
                    <div class="container">
                        <div class="row">
                            <form class="col-12 offset-md-3 col-md-6" method="POST" action="{{route('settings.update',['id' => $user->id])}}">
                                {{ csrf_field() }}
                                @method('PUT')
                                <div class="row justify-content-center">
                                    <div  class="col-12  account_settings_container">
                                        <div class="row">
                                            <div class="account_setting_header col-3">
                                                <h2>{{ __('Account') }}</h2>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="account_setting_name col-4">
                                               <label class="account_setting_name_label">{{ __('accountsettings.name')}}</label>
                                            </div>
                                            <div class="account_setting_name col-8">
                                                <input class="account_setting_input account_setting_name_input" value="{{$user->name}}" name="name">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="account_setting_mail col-4">
                                                <label class="account_setting_mail_label">{{ __('accountsettings.mail')}}</label>
                                            </div>
                                            <div class="account_setting_mail col-8">
                                                <label class="account_setting_mail_input">{{$user->email}}</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="account_setting_language col-4">
                                                <label class="account_setting_language_label">{{ __('accountsettings.language')}}</label>
                                            </div>
                                            <div class="account_setting_language col-8">
                                                <select class="account_setting_language_select" name="language" >
                                                    @switch($user->language)
                                                        @case('en')
                                                            <option class="account_setting_language_option" selected value="en">
                                                                <img class="lang_flag" src="{{asset('assets/flags/uk.png')}}"> English
                                                            </option>
                                                            <option class="account_setting_language_option" value="nl">
                                                                <img class="lang_flag" src="{{asset('assets/flags/nl.png')}}"> Dutch</a>
                                                            </option>
                                                        @break
                                                        @case('nl')
                                                            <option class="account_setting_language_option" value="en">
                                                                <img class="lang_flag" src="{{asset('assets/flags/uk.png')}}"> English
                                                            </option>
                                                            <option class="account_setting_language_option" selected value="nl">
                                                                <img class="lang_flag" src="{{asset('assets/flags/nl.png')}}"> Dutch</a>
                                                            </option>
                                                        @break
                                                        @case('de')
                                                        <option class="account_setting_language_option" selected>
                                                            <img class="lang_flag" src="{{asset('assets/flags/nl.png')}}"> Dutch</a>
                                                        </option>
                                                        @break
                                                        @case('in')
                                                            <option class="account_setting_language_option" selected>
                                                                <img class="lang_flag" src="{{asset('assets/flags/nl.png')}}"> Dutch</a>
                                                            </option>
                                                        @break
                                                        @case('fr')
                                                            <option class="account_setting_language_option" selected>
                                                                <img class="lang_flag" src="{{asset('assets/flags/nl.png')}}"> Dutch</a>
                                                            </option>
                                                        @break
                                                        @case('es')
                                                            <option class="account_setting_language_option" selected>
                                                                <img class="lang_flag" src="{{asset('assets/flags/nl.png')}}"> Dutch</a>
                                                            </option>
                                                        @break
                                                        @case('ch')
                                                            <option class="account_setting_language_option" selected>
                                                                <img class="lang_flag" src="{{asset('assets/flags/nl.png')}}"> Dutch</a>
                                                            </option>
                                                        @break
                                                        @default
                                                            <option class="account_setting_language_option" selected value="en">
                                                                <img class="lang_flag" src="{{asset('assets/flags/uk.png')}}"> English
                                                            </option>
                                                            <option class="account_setting_language_option" value="nl">
                                                                <img class="lang_flag" src="{{asset('assets/flags/nl.png')}}"> Dutch</a>
                                                            </option>
                                                    @endswitch
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="account_setting_password col-4">
                                                <label class="account_setting_password_label">{{ __('accountsettings.password')}}</label>
                                            </div>
                                            <div class="account_setting_password col-8">
                                                @if (Route::has('password.request'))
                                                    <a class="btn account_setting_reset_password" href="{{ route('password.request') }}">
                                                        {{ __('Reset') }}
                                                    </a>
                                                @endif

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="account_setting_mail_notification col-4">
                                                <label class="account_setting_mail_notification_label">{{ __('accountsettings.mail notification')}}</label>
                                            </div>
                                            <div class="account_setting_mail col-8">
                                                <label class="switch">
                                                    @if($user->mail_notification)
                                                        <input id="mail_notification_checked" type="checkbox" value="{{$user->mail_notification}}" checked name="mail_notification" value="1" onclick="setMailNot(this)">
                                                        <span class="slider round"></span>
                                                    @else
                                                        <input id="mail_notification" type="checkbox" value="{{$user->mail_notification}}" name="mail_notification" value="0" onclick="setMailNot(this)">
                                                        <span class="slider round"></span>
                                                    @endif
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-3">
                                                <button type="submit" class="account_save_btn  btn btn-primary">
                                                    {{ __('Save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>

        </div>



    </div>


@endsection