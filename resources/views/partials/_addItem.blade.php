

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary add_item_btn" data-toggle="modal" data-target="#addItemModal">
    +
</button>

<!-- Modal -->
<div class="add_new_item_modal modal fade" id="addItemModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title" id="exampleModalLabel">Create New Item</h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="add_new_item_form app-form" id="modal-form" action="{{route('home.store')}}" method="POST">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div id="" class="">
                        <div class="row">
                            <div class="container new_item_container">
                                <div class="row justify-content-center">
                                    <div class="col-12 col-xl-8 new_item_form">
                                        <div class="row new_item_row">
                                            <label class="new_item_title col-md-5">{{__('item.name')}}</label>
                                            <input id="new_item_name" class="new_item_input_text col-md-7" type="text" name="name"/>
                                        </div>
                                        <div class="row new_item_row">
                                            <label class="new_item_title col-md-5">{{__('item.type')}}</label>
                                            <select id="new_item_type" class="new_item_type_select col-md-7" type="text" name="type">
                                                @include("partials.filters._typeSelectOptions")
                                            </select>
                                        </div>
                                        <div class="row new_item_row">
                                            <label class="new_item_title col-md-5">{{__('item.entry date')}}</label>
                                            <input id="new_item_entry_date" class="new_item_input_date col-md-7" type="date" name="entryDate"/>
                                        </div>
                                        <div class="row new_item_row">
                                            <label class="new_item_title col-md-5">{{__('item.execution date')}}</label>
                                            <input id="new_item_execution_date" class="new_item_input_date col-md-7" type="date" name="executionDate"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn_close" data-dismiss="modal">{{__('item.close')}}</button>
                    <input type="submit" class="btn btn-primary btn_save" value="{{__('item.submit')}}">
                </div>
            </form>
        </div>
    </div>
</div>