<option class="new_item_type_option" value="Bread">{{ __('types.Bread')}}</option>
<option class="new_item_type_option" value="Dough">{{ __('types.Dough')}}</option>
{{--<option class="new_item_type_option" value="Divers">Divers</option>--}}
<option class="new_item_type_option" value="Fruit">{{ __('types.Fruit')}}</option>
<option class="new_item_type_option" value="Vegetable">{{ __('types.Vegetable')}}</option>
<option class="new_item_type_option" value="Chicken">{{ __('types.Chicken')}}</option>
<option class="new_item_type_option" value="Herbs">{{ __('types.Herbs')}}</option>
<option class="new_item_type_option" value="Pasta">{{ __('types.Pasta')}}</option>
<option class="new_item_type_option" value="Rice">{{ __('types.Rice')}}</option>
<option class="new_item_type_option" value="Saus">{{ __('types.Saus')}}</option>
<option class="new_item_type_option" value="Snacks">{{ __('types.Snacks')}}</option>
<option class="new_item_type_option" value="Soup">{{ __('types.Soup')}}</option>
<option class="new_item_type_option" value="Meat">{{ __('types.Meat')}}</option>
<option class="new_item_type_option" value="Ice">{{ __('types.Ice')}}</option>