<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;


class SendNotificationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
        $this->build();
    }



    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->view('view.name');

        $now = strtotime(date('Y-m-d'));
        $to = $now + 86400;
        $items = DB::table("fridge")
            ->where("user_id",$this->user->id)
            ->where("entry_date",$now)
            ->where("ex_date",$to)
            ->get();

        foreach ($items as $item){
            $item->entry_date =  date('d-m-Y', $item->entry_date);
            $item->ex_date =  date('d-m-Y', $item->ex_date);
        }
        echo "sendin mail";
        return $this->from('info@youngblood-it.com')
            ->view('view.partials.mail.mailnotification',['items' => $items]);
    }
}
