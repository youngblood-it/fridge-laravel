<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class HomeController extends Controller
{

    public $update_id = 0;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {


        $user = Auth::user();
       // var_dump($user);
        session()->put('locale', $user->language);
        App::setLocale($user->language);

        $items = DB::table("fridge")->where("user_id",Auth::id())->get();
        foreach ($items as $item){
            $item->entry_date =  date('d-m-Y', $item->entry_date);
            $item->ex_date =  date('d-m-Y', $item->ex_date);
        }
        return view('home',['items' => $items]);
    }

    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->input('name');
        $type = $request->input('type');
        $entry = $request->input('entryDate');
        $execution = $request->input('executionDate');

        $entry = strtotime(date($entry));
        $execution = strtotime(date($execution));

        if(!empty($request)) {
            DB::table('fridge')->insert([
                'user_id' => Auth::id(),
                'device_id' => 'WEB',
                'fcm_token' => 'WEB',
                'product_name' => $name,
                'product_type' => $type,
                'entry_date' => $entry,
                'ex_date' => $execution,
                'photo' => ""
            ]);
        }

        $request =  null;

        $locale = session()->get('locale');
        App::setLocale($locale);



        $items = DB::table("fridge")->where("user_id",Auth::id())->get();
        foreach ($items as $item){
            $item->entry_date =  date('d-m-Y', $item->entry_date);
            $item->ex_date =  date('d-m-Y', $item->ex_date);
        }
        return view('home',['items' => $items]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $locale = session()->get('locale');
        App::setLocale($locale);

        $items = DB::table("fridge")->where("id",$id)->get();
        foreach ($items as $item){
            $item->entry_date =  date('d-m-Y', $item->entry_date);
            $item->ex_date =  date('d-m-Y', $item->ex_date);
        }
        return $items;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->input('name');
        $type = $request->input('type');
        $entry = $request->input('entryDate');
        $execution = $request->input('executionDate');

        $entry = strtotime(date($entry));
        $execution = strtotime(date($execution));

        if(!empty($request)) {
            DB::table('fridge')->where('id', $id)->update([
                'id' => $id,
                'user_id' => Auth::id(),
                'device_id' => 'WEB',
                'fcm_token' => 'WEB',
                'product_name' => $name,
                'product_type' => $type,
                'entry_date' => $entry,
                'ex_date' => $execution,
                'photo' => ""
            ]);
        }

        $request = null;

        $locale = session()->get('locale');
        App::setLocale($locale);


        $items = DB::table("fridge")->where("user_id",Auth::id())->get();
        foreach ($items as $item){
            $item->entry_date =  date('d-m-Y', $item->entry_date);
            $item->ex_date =  date('d-m-Y', $item->ex_date);
        }
        return view('home',['items' => $items]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table("fridge")->delete($id);

        $locale = session()->get('locale');
        App::setLocale($locale);


        URL::to('/home');

        $items = DB::table("fridge")->where("user_id",Auth::id())->get();
        foreach ($items as $item){
            $item->entry_date =  date('d-m-Y', $item->entry_date);
            $item->ex_date =  date('d-m-Y', $item->ex_date);
        }
        return view('home',['items' => $items]);
    }

    public function filter(Request $request)
    {
        $type  = $request->input('type_filter');

        if($type == "All"){
            $items = DB::table("fridge")->where("user_id",Auth::id())->get();
        }else{
            $items = DB::table("fridge")->where("product_type",$type)->where("user_id",Auth::id())->get();
        }

        $locale = session()->get('locale');
        App::setLocale($locale);

        foreach ($items as $item){
            $item->entry_date =  date('d-m-Y', $item->entry_date);
            $item->ex_date =  date('d-m-Y', $item->ex_date);
        }
        return view('home',['items' => $items]);
    }




}
