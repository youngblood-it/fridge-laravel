<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SettingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();

        $locale = session()->get('locale');
        App::setLocale($locale);

        return view('partials._accountSettings', ['user' => $user]);
    }

    public function account(){
        $locale = session()->get('locale');
        App::setLocale($locale);

        $user = Auth::user();
        return view('partials._accountSettings', ['user' => $user]);
    }

    public function test(){
        $user = Auth::user();

        return view('partials._testSettings', ['user' => $user]);
    }


    public function update(Request $request, $id)
    {
        $name = $request->input('name');
        $lang = $request->input('language');
        $mail_notification = 0;

        if ( ! $request->has('mail_notification')) {
            $mail_notification = 0;
        }else{
            $mail_notification = 1;
        }

        DB::table('users')->where('id',$id)->update([
            'name'=>$name,
            'mail_notification'=>$mail_notification,
            'language'=>$lang,

        ]);



        App::setLocale($lang);
        session()->put('locale', $lang);

        return view('settings', ['user' => Auth::user()]);
    }
}
