<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Mail\SendNotificationMail;
use Illuminate\Support\Facades\Auth;
use App\User;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        //'App\Console\Commands\Test',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //TODO load user this is not working
        foreach (User::all() as $user) {
            if($user->mail_notification) {
                //$schedule->job(new SendWeeklyEmail($user))->weekly();
                $schedule->job(new SendNotificationMail($user))->everyMinute();
            }
        }


    }



    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }

}
